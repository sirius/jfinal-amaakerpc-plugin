package com.amaake.controller;

import com.amaake.rpc.client.AmaakeClientRpc;
import com.amaake.service.IndexService;
import com.jfinal.core.Controller;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/19 0019
 * \* Time: 11:02
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class IndexController extends Controller {

    private IndexService indexService = AmaakeClientRpc.getAesService(IndexService.class,"demoserver");

    public void index(){
        renderText("成功");
    }

    public void showaa(){
        indexService.showaa();
        renderText("showaa成功");
    }

    public void setaa(){
        indexService.setaa("给你一个参数aa");
        renderText("setaa成功");
    }

    public void getaa(){
        renderText(indexService.getaa());
    }


}