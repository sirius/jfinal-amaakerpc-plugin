package com.amaake.config;

import com.amaake.rpc.client.AmaakeClientRpcPlugin;
import com.amaake.rpc.client.Proxy;
import com.amaake.controller.IndexController;
import com.jfinal.config.*;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/19 0019
 * \* Time: 10:58
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class Config extends JFinalConfig {
    public void configConstant(Constants me) {

    }

    public void configRoute(Routes me) {
        me.add("/", IndexController.class);
    }

    public void configPlugin(Plugins me) {
        AmaakeClientRpcPlugin amaakeClientRpcPlugin = new AmaakeClientRpcPlugin();
        //下面的是直连服务端的例子
//        Proxy proxy = new Proxy();
//        proxy.setIp("127.0.0.1");
//        proxy.setPort(1688);
//        proxy.setPublickey("30819f300d06092a864886f70d010101050003818d0030818902818100c37d45da148968bb5c5dc89918f28e03b4879d2d485e95d3b5c960fa81e8dd4998a37fd8b97ce44f0ecfebbd6c33b1b4c65f163d9e4e5bf2031c10ab834ea3747045a030582c4a583e146aaef0d1bb42716ff691906171377cffcd76f889f07dbf5e07b7fc1d40eb489a1b230bc1c590ded03db66a55b759bc18c6ddda81e7070203010001");
//        amaakeClientRpcPlugin.addProxyMap("demoserver",proxy,false);
        //这是链接注册中心获取服务端信息的例子
        Proxy proxy = new Proxy();//注册中心的链接
        proxy.setIp("127.0.0.1");
        proxy.setPort(1888);
        proxy.setPublickey("30819f300d06092a864886f70d010101050003818d0030818902818100c37d45da148968bb5c5dc89918f28e03b4879d2d485e95d3b5c960fa81e8dd4998a37fd8b97ce44f0ecfebbd6c33b1b4c65f163d9e4e5bf2031c10ab834ea3747045a030582c4a583e146aaef0d1bb42716ff691906171377cffcd76f889f07dbf5e07b7fc1d40eb489a1b230bc1c590ded03db66a55b759bc18c6ddda81e7070203010001");
        amaakeClientRpcPlugin.addProxyMap("demoserver",proxy,true);//最后一个参数是开启链接注册中心
        me.add(amaakeClientRpcPlugin);
    }

    public void configInterceptor(Interceptors me) {

    }

    public void configHandler(Handlers me) {

    }
}