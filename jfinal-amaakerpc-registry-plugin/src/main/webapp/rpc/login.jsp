<%--
  Created by IntelliJ IDEA.
  User: Amaake
  Date: 2016/11/20 0020
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>RPC工作站登陆</title>
    <link rel="stylesheet" href="${rpcbase}/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${rpcbase}/css/login.css" />
</head>
<body class="beg-login-bg">
<div class="beg-login-box">
    <header>
        <h1>RPC登录</h1>
    </header>
    <div class="beg-login-main">
        <form action="" class="layui-form" method="post">
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe612;</i>
                </label>
                <input type="text" name="userName" lay-verify="required"  autocomplete="off" placeholder="这里输入登录名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="beg-login-icon">
                    <i class="layui-icon">&#xe642;</i>
                </label>
                <input type="password" name="password" lay-verify="required" autocomplete="off" placeholder="这里输入密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="beg-pull-right">
                    <button class="layui-btn layui-btn-primary" lay-submit lay-filter="login">
                        <i class="layui-icon">&#xe650;</i> 登录
                    </button>
                </div>
                <div class="beg-clear"></div>
            </div>
        </form>
    </div>
    <footer>
        <p>Amaakerpc © amaake</p>
    </footer>
</div>
<script src="${rpcbase}/plugins/layui/layui.js"></script>
<script>

    //一般直接写在一个js文件中
    layui.use(['layer', 'form','jquery'], function(){
        var layer = layui.layer
            ,form = layui.form()
            ,jquery = layui.jquery;
        //监听提交
        form.on('submit(login)', function(data){
            var layerload= layer.load(1);
            var state = '';
            var referer = '';

            jquery.ajax({
                url:'${rpcbase}/login',
                type:'POST', //GET
                async:true,    //或false,是否异步
                data:{
                    username:data.field.userName,password:data.field.password
                },
                timeout:5000,    //超时时间
                dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success:function(data,textStatus,jqXHR){
                    var icon = 1;
                    if(data.state=='success'){
                        icon = 1;
                    }else{
                        icon = 2;
                    }
                    layer.open({
                        icon: icon,
                        content: data.message,
                        success: function(layero, index){
                            layer.close(layerload);
                        },
                        yes: function(index, layero){
                            //do something
                            layer.close(index); //如果设定了yes回调，需进行手工关闭
                            window.location.href="${rpcbase}";
                        },
                        cancel: function(index){
                            layer.close(index)
                            window.location.href="${rpcbase}";
                        }
                    });
                },
                error:function(xhr,textStatus){
                    layer.open({
                        icon: 2,
                        content: '登陆出错，网络无法访问，或者系统错误，请联系管理员',
                        success: function(layero, index){
                            layer.close(layerload);
                        },
                        yes: function(index, layero){
                            //do something
                            layer.close(index); //如果设定了yes回调，需进行手工关闭
                            window.location.href="${rpcbase}";
                        },
                        cancel: function(index){
                            layer.close(index)
                            window.location.href="${rpcbase}";
                        }
                    });
                }
            });
            return false;
        });
    });
</script>

</body>
</html>
