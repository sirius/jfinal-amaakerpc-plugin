<%@ page import="com.amaake.rpc.client.Proxy" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: Amaake
  Date: 2016/11/27 0027
  Time: 17:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>控制面板</title>
    <link rel="stylesheet" href="${rpcbase}/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${rpcbase}/css/global.css" media="all">
    <link rel="stylesheet" href="${rpcbase}/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${rpcbase}/css/table.css" />
</head>

<body>
<div class="admin-main">

    <blockquote class="layui-elem-quote">
        信息：
    </blockquote>
    <fieldset class="layui-elem-field">
        <legend>服务端列表</legend>
        <div class="layui-field-box">
            <table class="site-table table-hover">
                <thead>
                <tr>
                    <th>别名</th>
                    <th>ip</th>
                    <th>端口</th>
                    <th>公钥</th>
                </tr>
                </thead>
                <tbody>
                <%
                    Map<String, Proxy> proxyMap = (Map<String, Proxy>)request.getAttribute("map");
                    for (Map.Entry entry : proxyMap.entrySet()) {
                        String key = entry.getKey().toString();
                        Proxy value = (Proxy)entry.getValue();
                 %>
                <tr>
                    <td><%=key%></td>
                    <td><%=value.getIp()%></td>
                    <td><%=value.getPort()%></td>
                    <td style='word-break:break-all;text-align:left'><%=value.getPublickey()%></td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>

        </div>
    </fieldset>
</div>
<script type="text/javascript" src="${rpcbase}/plugins/layui/layui.js"></script>
<script>var rpcbase = '${rpcbase}'</script>
<script>
    layui.config({
        base: rpcbase+'/plugins/layui/modules/'
    });

    layui.use(['icheck', 'laypage','layer'], function() {
        var $ = layui.jquery,
            laypage = layui.laypage,
            layer = parent.layer === undefined ? layui.layer : parent.layer;
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-green'
        });

        //page
        laypage({
            cont: 'page',
            pages: 25 //总页数
            ,
            groups: 5 //连续显示分页数
            ,
            jump: function(obj, first) {
                //得到了当前页，用于向服务端请求对应数据
                var curr = obj.curr;
                if(!first) {
                    //layer.msg('第 '+ obj.curr +' 页');
                }
            }
        });

        $('#search').on('click', function() {
            parent.layer.alert('你点击了搜索按钮')
        });

        $('#add').on('click', function() {
            $.get('temp/edit-form.html', null, function(form) {
                layer.open({
                    type: 1,
                    title: '添加表单',
                    content: form,
                    btn: ['保存', '取消'],
                    area: ['600px', '400px'],
                    maxmin: true,
                    yes: function(index) {
                        console.log(index);
                    },
                    full: function(elem) {
                        var win = window.top === window.self ? window : parent.window;
                        $(win).on('resize', function() {
                            var $this = $(this);
                            elem.width($this.width()).height($this.height()).css({
                                top: 0,
                                left: 0
                            });
                            elem.children('div.layui-layer-content').height($this.height() - 95);
                        });
                    }
                });
            });
        });

        $('#import').on('click', function() {
            var that = this;
            var index = layer.tips('只想提示地精准些', that,{tips: [1, 'white']});
            $('#layui-layer'+index).children('div.layui-layer-content').css('color','#000000');
        });

        $('.site-table tbody tr').on('click', function(event) {
            var $this = $(this);
            var $input = $this.children('td').eq(0).find('input');
            $input.on('ifChecked', function(e) {
                $this.css('background-color', '#EEEEEE');
            });
            $input.on('ifUnchecked', function(e) {
                $this.removeAttr('style');
            });
            $input.iCheck('toggle');
        }).find('input').each(function() {
            var $this = $(this);
            $this.on('ifChecked', function(e) {
                $this.parents('tr').css('background-color', '#EEEEEE');
            });
            $this.on('ifUnchecked', function(e) {
                $this.parents('tr').removeAttr('style');
            });
        });
        $('#selected-all').on('ifChanged', function(event) {
            var $input = $('.site-table tbody tr td').find('input');
            $input.iCheck(event.currentTarget.checked ? 'check' : 'uncheck');
        });
    });
</script>
</body>

</html>
