<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<html>

                <head>
                    <meta charset="utf-8">
                    <title>RPC工作台</title>
                    <meta name="renderer" content="webkit">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                    <meta name="apple-mobile-web-app-status-bar-style" content="black">
                    <meta name="apple-mobile-web-app-capable" content="yes">
                    <meta name="format-detection" content="telephone=no">

                    <link rel="stylesheet" href="${rpcbase}/plugins/layui/css/layui.css" media="all" />
                    <link rel="stylesheet" href="${rpcbase}/css/global.css" media="all">
                    <link rel="stylesheet" href="${rpcbase}/plugins/font-awesome/css/font-awesome.min.css">

                </head>

                <body>
                <div class="layui-layout layui-layout-admin">
                    <div class="layui-header header header-demo">
                        <div class="layui-main">
                            <div class="admin-login-box">
                <a class="logo" style="left: 0;" href="${rpcbase}">
                    <span style="font-size: 22px;">RPCAmaake</span>
                </a>
                <div class="admin-side-toggle">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </div>
            </div>
            <ul class="layui-nav admin-header-item">
                <li class="layui-nav-item">
                    <a href="javascript:;" class="admin-header-user">
                        <img src="${rpcbase}/images/0.jpg" />
                        <span>RPCadmin</span>
                    </a>
                    <dl class="layui-nav-child">
                        <dd>
                            <a href="#" id="quit"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
                        </dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav admin-header-item-mobile">
                <li class="layui-nav-item">
                    <a href="#" id="quit" ><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="layui-side layui-bg-black" id="admin-side">
        <div class="layui-side-scroll" id="admin-navbar-side" lay-filter="side"></div>
    </div>
    <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
        <div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
            <ul class="layui-tab-title">
                <li class="layui-this">
                    <i class="fa fa-dashboard" aria-hidden="true"></i>
                    <cite>控制面板</cite>
                </li>
            </ul>
            <div class="layui-tab-content" style="min-height: 150px; padding: 5px 0 0 0;">
                <div class="layui-tab-item layui-show">
                    <iframe src="${rpcbase}/rpchome"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-footer footer footer-demo" id="admin-footer">
        <div class="layui-main">
            <p>2016 &copy;
                <a href="http://www.amaake.com">www.amaake.com</a> license
            </p>
        </div>
    </div>
    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>
    <script type="text/javascript" src="${rpcbase}/plugins/layui/layui.js"></script>
    <script type="text/javascript" src="${rpcbase}/datas/nav.js" ></script>
    <script>var rpcbase = '${rpcbase}'</script>
    <script src="${rpcbase}/js/index.js"></script>
    <script>
        layui.use(['layer', 'form','jquery'], function(){
            var layer = layui.layer
                ,form = layui.form()
                ,jquery = layui.jquery;
            jquery("#quit").click(function () {
                var layerload= layer.load(1);
                var state = '';
                var referer = '';

                jquery.ajax({
                    url:'${rpcbase}/quit',
                    type:'POST', //GET
                    async:true,    //或false,是否异步
                    timeout:5000,    //超时时间
                    dataType:'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    success:function(data,textStatus,jqXHR){
                        var icon = 1;
                        if(data.state=='success'){
                            icon = 1;
                        }else{
                            icon = 2;
                        }
                        layer.open({
                            icon: icon,
                            content: data.message,
                            success: function(layero, index){
                                layer.close(layerload);
                            },
                            yes: function(index, layero){
                                //do something
                                layer.close(index); //如果设定了yes回调，需进行手工关闭
                                window.location.href="${rpcbase}";
                            },
                            cancel: function(index){
                                layer.close(index)
                                window.location.href="${rpcbase}";
                            }
                        });
                    },
                    error:function(xhr,textStatus){
                        layer.open({
                            icon: 2,
                            content: '登陆出错，网络无法访问，或者系统错误，请联系管理员',
                            success: function(layero, index){
                                layer.close(layerload);
                            },
                            yes: function(index, layero){
                                //do something
                                layer.close(index); //如果设定了yes回调，需进行手工关闭
                                window.location.href="${rpcbase}";
                            },
                            cancel: function(index){
                                layer.close(index)
                                window.location.href="${rpcbase}";
                            }
                        });
                    }
                });
                return false;
            })
        })
    </script>
</div>
</body>

</html>