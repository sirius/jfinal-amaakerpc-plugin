package com.amaake.rpc.registry;

import com.amaake.rpc.client.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/20 0020
 * \* Time: 15:42
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeRpcProxyMap {

    public static Map<String, Proxy> proxyMap = new HashMap<String, Proxy>();

    public static String path = "";

    public static RpcRegLoginBean rpcRegLoginBean = null;
}