package com.amaake.rpc.registry;

import com.amaake.rpc.registry.controller.RpcServiceController;
import com.jfinal.config.Routes;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/20 0020
 * \* Time: 17:15
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeRpcRegistryRoutes extends Routes {

    public void config() {
        add(AmaakeRpcProxyMap.path, RpcServiceController.class);
    }
}