package com.amaake.rpc.registry;

import com.amaake.rpc.client.Proxy;
import com.amaake.rpc.utils.RpcService;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/20 0020
 * \* Time: 15:44
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
@RpcService("rpcregistry")
public class RpcRegistryServiceImp implements RpcRegistryService {

    public void addProxy(String bm,Proxy proxy) {
        AmaakeRpcProxyMap.proxyMap.put(bm,proxy);
    }

    public Proxy getProxy(String bm) {
        return AmaakeRpcProxyMap.proxyMap.get(bm);
    }

    public void removeProxy(String bm){
        AmaakeRpcProxyMap.proxyMap.remove(bm);
    }

    public void showtext(){
        System.out.println("测试成功");
    }
}