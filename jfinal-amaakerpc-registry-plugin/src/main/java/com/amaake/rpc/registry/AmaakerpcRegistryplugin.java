package com.amaake.rpc.registry;

import com.amaake.rpc.server.AmaakeServerRpcPlugin;
import com.amaake.rpc.server.RpcServiceLists;
import com.jfinal.config.Routes;
import com.jfinal.plugin.IPlugin;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/20 0020
 * \* Time: 15:33
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakerpcRegistryplugin implements IPlugin {

    private int port = 12138;
    private String privatekey = "";
    private AmaakeServerRpcPlugin amaakeServerRpcPlugin = null;
    private Routes routes = null;
    private String path = "";
    private RpcRegLoginBean rpcRegLoginBean = null;

    public void setRpcRegLoginBean(RpcRegLoginBean rpcRegLoginBean) {
        this.rpcRegLoginBean = rpcRegLoginBean;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey;
    }

    public void setRoutes(Routes routes) {
        this.routes = routes;
    }

    public boolean start() {
        amaakeServerRpcPlugin = new AmaakeServerRpcPlugin();
        amaakeServerRpcPlugin.setPort(port);
        amaakeServerRpcPlugin.setActiveAuth(true);
        amaakeServerRpcPlugin.setPrivatekey(privatekey);
        RpcServiceLists rpcServiceLists = new RpcServiceLists();
        rpcServiceLists.setIsIfsearch(false);
        rpcServiceLists.addService(RpcRegistryServiceImp.class);
        amaakeServerRpcPlugin.setRpcServiceLists(rpcServiceLists);
        amaakeServerRpcPlugin.start();
        AmaakeRpcProxyMap.path = path;
        AmaakeRpcProxyMap.rpcRegLoginBean = rpcRegLoginBean;
        routes.add(new AmaakeRpcRegistryRoutes());
        return true;
    }

    public boolean stop() {
        amaakeServerRpcPlugin.stop();
        return true;
    }
}