package com.amaake.rpc.registry;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/20 0020
 * \* Time: 17:22
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class RpcLoginInterceptor implements Interceptor {

    public void intercept(Invocation inv) {
        StringBuffer url = inv.getController().getRequest().getRequestURL();
        String tempContextUrl = url.delete(url.length() - inv.getController().getRequest().getRequestURI().length(), url.length())
                .append(inv.getController().getRequest().getServletContext().getContextPath()).toString();
        inv.getController().setAttr("rpcbase",tempContextUrl+"/rpc");
        RpcRegLoginBean rpcRegLoginBean = inv.getController().getSessionAttr("rpcloginsession");
        if(rpcRegLoginBean == null){
            if(AmaakeRpcProxyMap.rpcRegLoginBean==null){
                rpcRegLoginBean = new RpcRegLoginBean();
                inv.getController().setSessionAttr("rpcloginsession",rpcRegLoginBean);
                inv.invoke();
            }else {
                inv.getController().renderJsp("/rpc/login.jsp");
            }
        }else{
            inv.invoke();
        }
    }
}