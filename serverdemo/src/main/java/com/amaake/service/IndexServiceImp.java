package com.amaake.service;

import com.amaake.rpc.utils.RpcService;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/19 0019
 * \* Time: 10:41
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
@RpcService("index")
public class IndexServiceImp implements IndexService {

    private static String aa="";

    public void showaa() {
        System.out.println("调用showaa成功");
    }

    public void setaa(String aa) {
        this.aa = aa;
    }

    public String getaa() {
        return aa;
    }
}