package com.amaake.config;

import com.amaake.controller.IndexController;
import com.amaake.rpc.client.Proxy;
import com.amaake.rpc.server.AmaakeServerRpcPlugin;
import com.amaake.rpc.server.RpcServiceLists;
import com.amaake.service.IndexServiceImp;
import com.jfinal.config.*;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/19 0019
 * \* Time: 10:38
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class Config extends JFinalConfig {

    private String publickey = "30819f300d06092a864886f70d010101050003818d0030818902818100c37d45da148968bb5c5dc89918f28e03b4879d2d485e95d3b5c960fa81e8dd4998a37fd8b97ce44f0ecfebbd6c33b1b4c65f163d9e4e5bf2031c10ab834ea3747045a030582c4a583e146aaef0d1bb42716ff691906171377cffcd76f889f07dbf5e07b7fc1d40eb489a1b230bc1c590ded03db66a55b759bc18c6ddda81e7070203010001";
    private String privatekey = "30820276020100300d06092a864886f70d0101010500048202603082025c02010002818100c37d45da148968bb5c5dc89918f28e03b4879d2d485e95d3b5c960fa81e8dd4998a37fd8b97ce44f0ecfebbd6c33b1b4c65f163d9e4e5bf2031c10ab834ea3747045a030582c4a583e146aaef0d1bb42716ff691906171377cffcd76f889f07dbf5e07b7fc1d40eb489a1b230bc1c590ded03db66a55b759bc18c6ddda81e707020301000102818067a266c088f89b7c10286baab5ee0859708020541c34806b3ec8e64f874522e2dc37791434a94003a54f96a2d1d4c9d593005fb304d477abe1f823e238f71c7134642474b711273c5086974a96f45f5e09b918b5aeaa765985c3a284c4f4cb14aafc85606b26b1cf8793a3c92c5b56d669f18e719429c5607f5f27458d20c449024100f98788d683267e3c537a60ba1cb0d4bf37cb7baf944fc40ec3cf858719f52446c4342c05433777615aa02d3b448e30b7fc2aea64dea2bd337d83117b0511455d024100c88f003ea38f42803b3fdbc6fed420764374c9e182a30c19d4bec6b6ec4c0abd523475691baf4e3fccfc9bd66e9037252c0176deba1a490e35ec1196c36293b30241008a7a2ef46ce025da30b18f90796fa10165636586b115fb45fe60e247719e5bbd72114a04b9a2c47ba0a197b876793a8c404348c525d0c69316aa703f0736e87902405c85ec1cf9a3ab9030c54304c8a6dfdaef3fbfaab373b8af6a0020f85e86ae25acf8da7984f618e9420dff6eab4f08c1b34d71a34ac3b9943eb80b5d3c020c150240683273dfc4784b79be59898ad06ee1169df09f4608d98fa7280db6c7a7d1ad340e45b2d2b494dc5a90a48e596fa7bd4791ab5c61ca280e03b42b876e6e7a660f";

    public void configConstant(Constants me) {

    }

    public void configRoute(Routes me) {
        me.add("/", IndexController.class);
    }

    public void configPlugin(Plugins me) {
        AmaakeServerRpcPlugin amaakeServerRpcPlugin = new AmaakeServerRpcPlugin();
        amaakeServerRpcPlugin.setPort(1688);
        amaakeServerRpcPlugin.setActiveAuth(true);
        amaakeServerRpcPlugin.setPrivatekey(privatekey);
        RpcServiceLists rpcServiceLists = new RpcServiceLists();
        rpcServiceLists.setIsIfsearch(false);
        rpcServiceLists.addService(IndexServiceImp.class);
        amaakeServerRpcPlugin.setRpcServiceLists(rpcServiceLists);

        //向注册中心注册
        amaakeServerRpcPlugin.setIfRegistry(true);//开启注册
        amaakeServerRpcPlugin.setPublickey(publickey);//客户端用的公钥
        amaakeServerRpcPlugin.setRegistryBm("serverdemo");//该服务端的别名
        Proxy proxy = new Proxy();//注册中心的服务端bean
        proxy.setIp("127.0.0.1");
        proxy.setPort(1888);
        proxy.setPublickey(publickey);
        amaakeServerRpcPlugin.setRegistryProxy(proxy);
        me.add(amaakeServerRpcPlugin);
    }

    public void configInterceptor(Interceptors me) {

    }

    public void configHandler(Handlers me) {

    }
}