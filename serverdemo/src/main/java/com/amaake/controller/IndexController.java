package com.amaake.controller;

import com.jfinal.core.Controller;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/19 0019
 * \* Time: 10:39
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class IndexController extends Controller {

    public void index(){
        renderText("成功");
    }
}