package com.amaake.rpc.server;

import com.amaake.rpc.client.Proxy;
import com.amaake.rpc.utils.RpcRegistryService;
import com.amaake.rpc.utils.RpcService;
import com.jfinal.plugin.IPlugin;
import link.jfire.baseutil.StringUtil;
import link.jfire.simplerpc.client.RpcFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/15 0015
 * \* Time: 20:42
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeServerRpcPlugin implements IPlugin {

    private int port=8080;
    private boolean activeAuth = false;
    private String privatekey = "";
    private RpcServiceLists rpcServiceLists = null;
    private AmaakeServerRpc amaakeServerRpc = null;
    private RpcRegistryService rpcRegistryService = null;

    //是否链接注册中心
    private boolean ifRegistry = false;
    private String registryBm = "";
    private String publickey = "";
    private Proxy registryProxy = null;

    public void setRegistryBm(String registryBm) {
        this.registryBm = registryBm;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }

    public void setIfRegistry(boolean ifRegistry) {
        this.ifRegistry = ifRegistry;
    }

    public void setRegistryProxy(Proxy registryProxy) {
        this.registryProxy = registryProxy;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setActiveAuth(boolean activeAuth) {
        this.activeAuth = activeAuth;
    }

    public void setPrivatekey(String privatekey) {
        this.privatekey = privatekey;
    }

    public void setRpcServiceLists(RpcServiceLists rpcServiceLists) {
        this.rpcServiceLists = rpcServiceLists;
    }

    public boolean start() {
        rpcServiceLists.init();
        amaakeServerRpc = new AmaakeServerRpc(port, activeAuth,  privatekey , rpcServiceLists);
        amaakeServerRpc.start();
        if(ifRegistry){
            RpcService rpcService = (RpcService) RpcRegistryService.class.getAnnotation(RpcService.class);
            rpcRegistryService = RpcFactory.getAesProxy(RpcRegistryService.class,rpcService.value(), StringUtil.hexStringToBytes(registryProxy.getPublickey()),registryProxy.getIp(),registryProxy.getPort(),null);
            Proxy bdproxy = new Proxy();
            bdproxy.setPort(port);
            String ip = "";
            try {
                Enumeration<NetworkInterface> interfaces=null;
                interfaces = NetworkInterface.getNetworkInterfaces();
                NetworkInterface ni = interfaces.nextElement();
                Enumeration<InetAddress> addresss = ni.getInetAddresses();
                InetAddress nextElement = addresss.nextElement();
                ip = nextElement.getHostAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
            bdproxy.setIp(ip);
            bdproxy.setPublickey(publickey);
            rpcRegistryService.showtext();
            rpcRegistryService.addProxy(registryBm,bdproxy);
        }
        return true;
    }

    public boolean stop() {
        amaakeServerRpc.stop();
        if(ifRegistry) {
            rpcRegistryService.removeProxy(registryBm);
        }
        return true;
    }
}