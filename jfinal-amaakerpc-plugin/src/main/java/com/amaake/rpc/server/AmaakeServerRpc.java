package com.amaake.rpc.server;

import link.jfire.baseutil.StringUtil;
import link.jfire.simplerpc.server.RcConfig;
import link.jfire.simplerpc.server.RcServer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/15 0015
 * \* Time: 21:28
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeServerRpc {

    private RcServer rcServer = null;
    private boolean activeAuth = false;
    private int port = 8080;
    private Map<String, Object> proxyMaps = null;
    private String privatekey = null;
    private RpcServiceLists serviceLists = null;

    public AmaakeServerRpc(int port,boolean activeAuth, String privatekey ,RpcServiceLists serviceLists){
        this.port = port;
        this.activeAuth = activeAuth;
        this.privatekey = privatekey;
        this.serviceLists = serviceLists;
        proxyMaps = serviceLists.getProxyMaps();
        init();
    }

    private void init(){
        //rpc服务端的配置类
        RcConfig rcConfig = new RcConfig();
        //设置监听端口
        rcConfig.setPort(port);
        //设置代理名称，这个功能有点类似命名空间，通过命名空间来区分同名方法
        List<String> proxyNames = new ArrayList<String>();
        List<Object> proxyvalues = new ArrayList<Object>();
        for(String key : proxyMaps.keySet()) {
            proxyNames.add(key);
        }
        for (Object value : proxyMaps.values()) {
            proxyvalues.add(value);
        }
        rcConfig.setProxyNames(proxyNames.toArray(new String[proxyNames.size()]));
        rcConfig.setImpls(proxyvalues.toArray(new Object[proxyvalues.size()]));
        //设置被开放为rpc调用的实例对象
        if(activeAuth){
            rcConfig.activeAuth();
            rcConfig.setPrivateKey(StringUtil.hexStringToBytes(privatekey));
        }
        System.out.println(rcConfig.isParamOk());
        rcServer = new RcServer(rcConfig);
    }


    public boolean start() {
        rcServer.start();
        return true;
    }

    public boolean stop() {
        rcServer.stop();
        return true;
    }
}