package com.amaake.rpc.utils;


import java.lang.annotation.*;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/15 0015
 * \* Time: 22:38
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface RpcService {
    String value();
}