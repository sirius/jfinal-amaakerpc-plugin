package com.amaake.rpc.utils;

/**
 * Created by Amaake on 2016/11/16 0016.
 */
public interface ClassFilter {
    boolean accept(Class clazz);
}
