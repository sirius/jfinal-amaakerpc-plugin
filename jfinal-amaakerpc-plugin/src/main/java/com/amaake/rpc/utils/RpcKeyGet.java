package com.amaake.rpc.utils;

import link.jfire.baseutil.encrypt.RSAUtil;

import java.io.IOException;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/18 0018
 * \* Time: 20:18
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class RpcKeyGet {

    public void buildkey(){
        RSAUtil rsaUtil = new RSAUtil();
        try {
            rsaUtil.buildKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}