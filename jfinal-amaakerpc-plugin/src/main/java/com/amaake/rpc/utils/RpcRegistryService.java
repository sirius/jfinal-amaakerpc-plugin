package com.amaake.rpc.utils;

import com.amaake.rpc.client.Proxy;

/**
 * Created by Amaake on 2016/11/20 0020.
 */
@RpcService("rpcregistry")
public interface RpcRegistryService {

    public void addProxy(String bm, Proxy proxy);

    public Proxy getProxy(String bm);

    public void removeProxy(String bm);

    public void showtext();
}
