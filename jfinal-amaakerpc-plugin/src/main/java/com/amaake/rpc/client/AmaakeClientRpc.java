package com.amaake.rpc.client;

import com.amaake.rpc.utils.RpcService;
import link.jfire.baseutil.StringUtil;
import link.jfire.simplerpc.client.RpcFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/18 0018
 * \* Time: 20:29
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeClientRpc {

    private static Map<String, Proxy> proxyMap = new HashMap<String, Proxy>();

    public void setProxyMap(Map<String, Proxy> proxyMap){
        this.proxyMap = proxyMap;
    }

    public static <T> T getAesService(Class<T> interfaceClass, String proxyName){
        Proxy proxy = proxyMap.get(proxyName);
        RpcService rpcService = (RpcService) interfaceClass.getAnnotation(RpcService.class);
        return RpcFactory.getAesProxy(interfaceClass,rpcService.value(), StringUtil.hexStringToBytes(proxy.getPublickey()),proxy.getIp(),proxy.getPort(),null);
    }

    public static <T> T getService(Class<T> interfaceClass, String proxyName){
        Proxy proxy = proxyMap.get(proxyName);
        RpcService rpcService = (RpcService) interfaceClass.getAnnotation(RpcService.class);
        return RpcFactory.getProxy(rpcService.value(),interfaceClass,proxy.getIp(),proxy.getPort());
    }

}