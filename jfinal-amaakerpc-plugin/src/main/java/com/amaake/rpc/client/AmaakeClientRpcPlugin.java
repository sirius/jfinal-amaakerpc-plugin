package com.amaake.rpc.client;

import com.amaake.rpc.utils.RpcRegistryService;
import com.amaake.rpc.utils.RpcService;
import com.jfinal.plugin.IPlugin;
import link.jfire.baseutil.StringUtil;
import link.jfire.simplerpc.client.RpcFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/18 0018
 * \* Time: 20:21
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class AmaakeClientRpcPlugin implements IPlugin {

    private Map<String, Proxy> proxyMap = new HashMap<String, Proxy>();

    public void addProxyMap(String bm,Proxy proxy,boolean ifregistry){
        if(ifregistry){
            RpcService rpcService = (RpcService) RpcRegistryService.class.getAnnotation(RpcService.class);
            RpcRegistryService rpcRegistryService = RpcFactory.getAesProxy(RpcRegistryService.class,rpcService.value(), StringUtil.hexStringToBytes(proxy.getPublickey()),proxy.getIp(),proxy.getPort(),null);
            proxyMap.put(bm,rpcRegistryService.getProxy(bm));
        }else {
            proxyMap.put(bm, proxy);
        }
    }

    public boolean start() {
        AmaakeClientRpc amaakeClientRpc = new AmaakeClientRpc();
        amaakeClientRpc.setProxyMap(proxyMap);
        return true;
    }

    public boolean stop() {
        return true;
    }
}