package com.amaake.rpc.client;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: Amaake
 * \* Date: 2016/11/18 0018
 * \* Time: 20:25
 * \* Description: 春眠不觉晓，起来敲代码。
 * \
 */
public class Proxy {

    private int port;
    private String ip;
    private String publickey;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPublickey() {
        return publickey;
    }

    public void setPublickey(String publickey) {
        this.publickey = publickey;
    }
}