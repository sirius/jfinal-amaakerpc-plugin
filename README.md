#jfinal-amaakerpc-plugin

>这是一个jfinal的插件，但是也可以单独使用，是我基于[eric_1989](https://git.oschina.net/eric_ds)大神的[jfire-simplerpc](https://git.oschina.net/eric_ds/jfire-simplerpc)的一个扩展，主要增加对jfinal的支持，增加扫描包功能等等，详细如下：

>[jfire-simplerpc](https://git.oschina.net/eric_ds/jfire-simplerpc)是一个简单快速的java间rpc调用 同时支持公网安全调用，采用rsa1024位握手加密，aes128位通讯加密

####server端rpc服务开启：
>
```java
AmaakeServerRpcPlugin amaakeServerRpcPlugin = new AmaakeServerRpcPlugin();
amaakeServerRpcPlugin.setPort(1688);//端口
amaakeServerRpcPlugin.setActiveAuth(true);//是否开启加密传输，可空
amaakeServerRpcPlugin.setPrivatekey(privatekey);//私钥，如果activeauth为false或者不赋值，可不用
RpcServiceLists rpcServiceLists = new RpcServiceLists();//Service列表实例
rpcServiceLists.setIsIfsearch(false);//是否开启class扫描
rpcServiceLists.addIncludePaths("com.amaake.service"); //添加扫描路径，可多路径
rpcServiceLists.addService(IndexServiceImp.class);//手动添加
amaakeServerRpcPlugin.setRpcServiceLists(rpcServiceLists);//将Service列表添加到服务里
###以下例子是连接注册中心时需要的###
//向注册中心注册
amaakeServerRpcPlugin.setIfRegistry(true);//开启注册
amaakeServerRpcPlugin.setPublickey(publickey);//客户端用的公钥
amaakeServerRpcPlugin.setRegistryBm("serverdemo");//该服务端的别名
Proxy proxy = new Proxy();//注册中心的服务端bean
proxy.setIp("127.0.0.1");//注册中心的ip
proxy.setPort(1888);//注册中心的端口
proxy.setPublickey(publickey);//连接注册中心需要的公钥
amaakeServerRpcPlugin.setRegistryProxy(proxy);
me.add(amaakeServerRpcPlugin);//启动，这是jfinal里插件的安装，你也可以amaakeServerRpcPlugin.start();直接启动
```
  
####client端：
>
```java
AmaakeClientRpcPlugin amaakeClientRpcPlugin = new AmaakeClientRpcPlugin();
Proxy proxy = new Proxy();//这是一个服务端连接实体，也可以是注册中心的连接
proxy.setIp("127.0.0.1");//服务端ip地址
proxy.setPort(1688);//ip地址
proxy.setPublickey(publickey);//公钥
amaakeClientRpcPlugin.addProxyMap("demoserver",proxy,true);////最后一个参数是是否开启链接注册中心
//添加到client连接池里，这里需要一个别名来区分服务端连接，如果是注册中心别名为注册中心中的服务端别名
me.add(amaakeClientRpcPlugin);//启动，jfinal方式，可用start()方法启动
###下面是在业务中使用的例子###
IndexService indexService = AmaakeClientRpc.getAesService(IndexService.class,"demoserver");
//AmaakeClientPpc是Service调用类，IndexService.class是Service的接口，"demoserver"是启动时的服务端别名，用来区分这个service
//是属于哪个服务端的，这里强调一个点getAesService方法是用来使用调用加密传输的service，getService方法是调用非加密传输
```

####Service写法
>主要为了藕解，使用service别名和服务端别名，一个客户端可以连接多个服务端
>服务端
```java
###接口###
public interface IndexService {
    public void showaa();
    public void setaa(String aa);
    public String getaa();
}
###接口实现###
@RpcService("index")//Service别名
public class IndexServiceImp implements IndexService {
    private static String aa="";
    public void showaa() {
        System.out.println("调用showaa成功");
    }
    public void setaa(String aa) {
        this.aa = aa;
    }
    public String getaa() {
        return aa;
    }
}
```
>客户端的接口
```java
@RpcService("index")//服务端service别名
public interface IndexService {
    public void showaa();
    public void setaa(String aa);
    public String getaa();
}
```

####注册中心写法
>注册中心
```java
###注意只支持jfinal###
private Routes routes = null;
public void configConstant(Constants me) {
}
public void configRoute(Routes me) {
    this.routes = me;获取Routes
}
public void configPlugin(Plugins me) {
    AmaakerpcRegistryplugin amaakerpcRegistryplugin = new AmaakerpcRegistryplugin();
    amaakerpcRegistryplugin.setPort(1888);注册中心的端口
    //注册中心的秘钥
    amaakerpcRegistryplugin.setPrivatekey("30820276020100300d06092a864886f70d0101010500048202603082025c02010002818100c37d45da148968bb5c5dc89918f28e03b4879d2d485e95d3b5c960fa81e8dd4998a37fd8b97ce44f0ecfebbd6c33b1b4c65f163d9e4e5bf2031c10ab834ea3747045a030582c4a583e146aaef0d1bb42716ff691906171377cffcd76f889f07dbf5e07b7fc1d40eb489a1b230bc1c590ded03db66a55b759bc18c6ddda81e707020301000102818067a266c088f89b7c10286baab5ee0859708020541c34806b3ec8e64f874522e2dc37791434a94003a54f96a2d1d4c9d593005fb304d477abe1f823e238f71c7134642474b711273c5086974a96f45f5e09b918b5aeaa765985c3a284c4f4cb14aafc85606b26b1cf8793a3c92c5b56d669f18e719429c5607f5f27458d20c449024100f98788d683267e3c537a60ba1cb0d4bf37cb7baf944fc40ec3cf858719f52446c4342c05433777615aa02d3b448e30b7fc2aea64dea2bd337d83117b0511455d024100c88f003ea38f42803b3fdbc6fed420764374c9e182a30c19d4bec6b6ec4c0abd523475691baf4e3fccfc9bd66e9037252c0176deba1a490e35ec1196c36293b30241008a7a2ef46ce025da30b18f90796fa10165636586b115fb45fe60e247719e5bbd72114a04b9a2c47ba0a197b876793a8c404348c525d0c69316aa703f0736e87902405c85ec1cf9a3ab9030c54304c8a6dfdaef3fbfaab373b8af6a0020f85e86ae25acf8da7984f618e9420dff6eab4f08c1b34d71a34ac3b9943eb80b5d3c020c150240683273dfc4784b79be59898ad06ee1169df09f4608d98fa7280db6c7a7d1ad340e45b2d2b494dc5a90a48e596fa7bd4791ab5c61ca280e03b42b876e6e7a660f");
    amaakerpcRegistryplugin.setPath("/rpc");//访问注册中心的path
    amaakerpcRegistryplugin.setRoutes(routes);//添加Routes
    RpcRegLoginBean rpcRegLoginBean = new RpcRegLoginBean();//登陆账号密码，不添加时打开不需要输入账号密码
    rpcRegLoginBean.setUsername("amaake");
    rpcRegLoginBean.setPassword("123456");
    amaakerpcRegistryplugin.setRpcRegLoginBean(rpcRegLoginBean);
    me.add(amaakerpcRegistryplugin);
}
```

####最后关于公钥密钥的生成，专门提供了方法
>生成秘钥对，输出到控制台上
```java
public static void main(String[] args){
    new RpcKeyGet().buildkey();//获取秘钥和公钥
}
```
>其他的可以见例子

>这是我心中的微服务架构，剩下的功能，我会围绕这个图，来做
 
![](rpc.png)
